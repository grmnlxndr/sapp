package com.german.sapp.service;

import com.german.sapp.domain.Product;

import java.io.Serializable;
import java.util.List;

/**
 * Created by developer on 18/09/15.
 */
public interface ProductManager extends Serializable {

    public void increasePrice(int percentage);

    public List<Product> getProducts();

}
